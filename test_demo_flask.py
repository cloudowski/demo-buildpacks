import unittest
from flask import current_app
from app import app
import os

class TestFlaskDemo(unittest.TestCase):
    def setUp(self):
        self.app = app
        self.appctx = self.app.app_context()
        self.appctx.push()
        self.client = self.app.test_client()

    def tearDown(self):
        self.appctx.pop()
        self.app = None
        self.appctx = None

    def test_app(self):
        assert self.app is not None
        assert current_app == self.app

    def test_api_stats(self):
        response = self.client.get('/api/stats', follow_redirects=True)
        print(f"{response.data}")
        assert response.status_code == 200
    
    def test_healthz(self):
        response = self.client.get('/healthz', follow_redirects=True)
        print(f"{response}")
        assert response.status_code == 200
    
    def test_v(self):
        response = self.client.get('/v', follow_redirects=True)
        print(f"{response.data}")
        assert "0.2.0" in response.data.decode("utf-8")
    
    def test_index(self):
        response = self.client.get('/', follow_redirects=True)
        print(f"{response.status_code}")
        assert response.status_code == 200
    
    def test_index_count(self):
        response = self.client.get('/', follow_redirects=True)
        response = self.client.get('/', follow_redirects=True)
        print(f"{response.data}")
        assert ": 3</h3>" in response.data.decode("utf-8")
    
class TestFlaskDemoConfig(unittest.TestCase):
    def setUp(self):
        self.app = app
        self.appctx = self.app.app_context()
        self.appctx.push()
        self.client = self.app.test_client()

    def tearDown(self):
        self.appctx.pop()
        self.app = None
        self.appctx = None

    def test_output_default(self):
        assert self.app.config['OUTPUT'] == 'html'
    
    def test_output_html(self):
        response = self.client.get('/', follow_redirects=True)
        print(f"{response.data}")
        assert "</h3>" in response.data.decode("utf-8")

    def test_output_text(self):
        self.app.config['OUTPUT'] = 'text'
        response = self.client.get('/', follow_redirects=True)
        print(f"{response.data}")
        assert "</h3>" not in response.data.decode("utf-8")
    