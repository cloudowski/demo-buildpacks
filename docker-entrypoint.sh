#!/bin/sh

# set -x

if [ "$REDIS_HOST" ];then
    t=60
    echo -n "[docker-entrypoint.sh] Wating ${t}s for redis insance [${REDIS_HOST}:6379]..."
    while ! nc -zv $REDIS_HOST 6379; do
        sleep 3
        let t="$t-3"
        if [ $t -le 0 ];then
            echo "[docker-entrypoint.sh] Timeout reached waiting for redis" >&2
            exit 1
        fi
    done
    echo "available"
else
    echo "[docker-entrypoint.sh] REDIS_HOST env undefined - starting the app without wating for the redis instance" >&2
fi

exec "$@"