#!/usr/bin/env python
from flask import Flask
from flask import jsonify
from redis import Redis
from redis.cluster import RedisCluster
import socket
import os
import requests
import toml
import logging
from kubernetes import client, config

from opentelemetry import trace
from opentelemetry import metrics
from opentelemetry.instrumentation.flask import FlaskInstrumentor

app = Flask(__name__)
app.config.from_file("config.toml", load=toml.load)
app.config.from_prefixed_env()

tracer = trace.get_tracer("demoflask.tracer")
meter = metrics.get_meter("demoflask.meter")
FlaskInstrumentor().instrument_app(app)

visit_counter = meter.create_counter(
    "demoflask.visits",
    description="The number of visits value",
)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("demoflask")

host = socket.gethostname()
VERSION = "0.3.0"
COUNTER = 0

try:
    redis = RedisCluster(host=os.environ.get('REDIS_HOST', 'redis'), port=6379, socket_connect_timeout=3)
except:
    redis = Redis(host=os.environ.get('REDIS_HOST', 'redis-master'), port=6379, socket_connect_timeout=3)

try: 
    redis.ping()
except:
    redis = False

def get_pods():
    with tracer.start_as_current_span("k8s_connect") as demoflask_span:
        pods = []
        if 'KUBERNETES_PORT' in os.environ:
            config.load_incluster_config()
        else:
            try:
                config.load_kube_config()
            except:
                return pods

        with tracer.start_as_current_span("k8s_get_pods") as demoflask_span:
            ns = os.environ.get('NAMESPACE', 'default')
            label_selector = 'app.kubernetes.io/name=demo-flask'

            v1 = client.CoreV1Api()
            logger.info("Listing pods with their IPs:")
            ret = v1.list_namespaced_pod(namespace=ns,label_selector=label_selector)

            for i in ret.items:
                p = { "ip": i.status.pod_ip, "name": i.metadata.name }
                pods.append(p)
                logger.debug(p)

            return pods

def inc_counter():
    with tracer.start_as_current_span("counter") as demoflask_span:
        global COUNTER
        if redis:
            COUNTER = redis.get(host)
            COUNTER = 0 if COUNTER == None else int(COUNTER)
            redis.set(host,COUNTER+1)
        COUNTER += 1
        return COUNTER

def get_app_version(pod_ip):
    link = f"http://{pod_ip}:8080/v"
    try:
        f = requests.get(link,timeout=0.2)
    except:
        return ""
    return f.text

# get stats from redis when available + include active pods in the list
def get_stats():
    with tracer.start_as_current_span("get_stats") as demoflask_span:
        stats = []
        redis_pods = []
        active_pods = get_pods()
        active_pods_added = {pod['name']:False for pod in active_pods}
        if redis:
            with tracer.start_as_current_span("count_stats_from_redis") as demoflask_span:
                # read from redis into a list
                for h in redis.scan()[1]:
                    redis_pods.append({"name": str(h,"utf-8"), "hits": int(redis.get(h))})
                # loop over pods from redis and k8s api
                for pod in redis_pods + active_pods:
                    # skip if already addded
                    if active_pods_added.get(pod['name'], False):
                        continue
                    # check if active - relevant for pods from redis
                    active = pod['name'] in active_pods_added.keys()
                    pod_ip = ""
                    if active:
                        # somewhat too complex way to retrieve ip from the pod list from k8s api
                        pod_ip = [p['ip'] for p in active_pods if p['name'] == pod['name']][0]
                        # skip this pod, as it's been added already
                        active_pods_added[pod['name']] = True
                    pod_app_version = get_app_version(pod_ip) if pod_ip else ""
                    hits = pod.get('hits',0)
                    stats.append({
                        "host": pod['name'],
                        "hits": hits,
                        "active": active,
                        "ip": pod_ip,
                        "app_version": pod_app_version
                        })
        else:
            with tracer.start_as_current_span("count_stats_from_k8s") as demoflask_span:
                # return active pods only if redis connection is missing
                for p in active_pods:
                    stats.append({
                        "host": p['name'],
                        "hits": 0,
                        "active": True,
                        "ip": p['ip'],
                        "app_version": get_app_version(p['ip'])
                        })

        # return sorted list by the pod name
        return sorted(stats, key=lambda d: d['host']) 

@app.route('/pods')
def pods():
    return jsonify(get_pods())

@app.route('/')
def index():
    with tracer.start_as_current_span("visit") as demoflask_span:
        msg = f"Visit for {host}: {str(inc_counter())}"
        demoflask_span.set_attribute("visits.value", COUNTER)
        visit_counter.add(COUNTER)
        if app.config['OUTPUT'] == 'html':
            msg = f"""
            <!DOCTYPE html>
            <html>
                <body style="background-color:{app.config['HTML_BG_COLOR']};">
                    <h3>{msg}</h3>
                </body>
            </html>
            """
        return msg

@app.route('/stats')
def stats():
    with tracer.start_as_current_span("stats") as demoflask_span:
        msg = ""
        if not redis:
            return "<h2>Stats available only with Redis</h2>"
        for d in get_stats():
            msg += f"<h5> {d['host']}:  {d['hits']}</h5>\n"
        msg += f"<p>Redis version: {redis.info().get('redis_version')}</p>"
        return msg

@app.route('/api/stats')
def api_stats():
    return jsonify(get_stats())

@app.route('/api/hit')
def api_hits():
    return jsonify({host: inc_counter()})

@app.route('/healthz')
def healthz():
    return "OK"

@app.route('/v')
def version():
    return VERSION

def create_app():
    return app

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=app.config['PORT'], debug=True, use_reloader=False)