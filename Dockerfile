FROM python:3.11-alpine AS builder

WORKDIR /code

COPY requirements.txt .
RUN pip3 install -r requirements.txt && pip3 cache purge

COPY . .

ENTRYPOINT ["python3"]
CMD ["app.py"]